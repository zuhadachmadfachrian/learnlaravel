@extends('layout/main')

@section('title', 'mahasiswa')
@section('isi')

<div class="container">
    <div class="row">
        <div class="col-md-10 mt-3">
           <h1>Data Mahasiswa</h1>
           <table class="table table-striped">
            <thead class="thead-dark">
              <tr>
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col">NIM</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                  @foreach ($mahasiswa as $mhs)
              <th scope="row">{{$loop->iteration}}</th>
                  <td>{{$mhs->nama}}</td>
                  <td>{{$mhs->nim}}</td>
                  <td>
                      <a href="" class="badge badge-success">edit</a>
                      <a href="" class="badge badge-danger">delete</a>
                  </td>
                </tr>
                  @endforeach
            </tbody>
          </table>
        </div>
    </div>
</div>
@endsection

