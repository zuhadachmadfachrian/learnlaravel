@extends('layout/main')

@section('title', 'Form Tambah Data Mahasiswa')
@section('isi')

<div class="container">
    <div class="row">
        <div class="col-md-10 mt-3">
           <h1>Form Tambah Data Mahasiswa</h1>
            <form method="post" action="/students" class="col-md-6 mt-3">
                @csrf
                <div class="form-group">
                    <label for="nama">Nama</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" value="{{old('name')}}" id="name" name="name" >
                    @error('name')
                    <div class="invalid-feedback">{{$message}}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nim">NIM</label>
                    <input type="number" class="form-control  @error('nim') is-invalid @enderror" value="{{old('nim')}}" id="nim" name="nim" >
                    @error('nim')
                    <div class="invalid-feedback">{{$message}}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="/students" class="btn btn-light">Kembali</a>
            </form>
        </div>
    </div>
</div>
@endsection

