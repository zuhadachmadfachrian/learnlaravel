@extends('layout/main')

@section('title', 'mahasiswa')
@section('isi')

<div class="container">
    <div class="row">
        <div class="col-md-10 mt-3">
           <h1>Data Mahasiswa</h1>
            <div class="col-md-4 mt-2 mb-2">
                <a class="btn btn-primary" href="/students/create">Tambah Data Mahasiswa</a>
            </div>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if (session('danger'))
                <div class="alert alert-danger">
                    {{ session('danger') }}
                </div>
            @endif
           <table class="table table-striped">
            <thead class="thead-dark">
              <tr>
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                  @foreach ($student as $stu)
              <th scope="row">{{$loop->iteration}}</th>
                  <td>{{$stu->nama}}</td>
                  <td>
                  <a href="/students/{{$stu->id}}" class="badge badge-success">detail</a>
                  </td>
                </tr>
                  @endforeach
            </tbody>
          </table>
        </div>
    </div>
</div>
@endsection

