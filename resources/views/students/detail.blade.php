@extends('layout/main')

@section('title', 'mahasiswa')
@section('isi')

<div class="container">
    <div class="row">
        <div class="col-md-6 mt-3">
           <h1>Data Mahasiswa</h1>
           <div class="card">
                <div class="card-body">
                <h5 class="card-title">{{$student->nama}}</h5>
                <h6 class="card-subtitle mb-2 text-muted">{{$student->nim}}</h6>
                <br>
                <a href="{{$student->id}}/edit" class="btn btn-success">Edit</a>
                <form action="{{$student->id}}" method="post" class="d-inline">
                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
              <a href="/students" class="card-link">Kembali</a>
           </div>
          </div>
        </div>
    </div>
</div>
@endsection

